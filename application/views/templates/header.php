<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link href='<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css' rel='stylesheet' type='text/css'>
	<link href='<?php echo base_url(); ?>assets/custom.css' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/jquery-ui.css">
	<script src='<?php echo base_url(); ?>assets/jquery.js' type='text/javascript'></script>
	<script src='<?php echo base_url(); ?>assets/jquery-ui.js' type='text/javascript'></script>
	<script src='<?php echo base_url(); ?>assets/jquery.validate.js' type='text/javascript'></script>
	<script src='<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js' type='text/javascript'></script>

	<title></title>
</head>
<body>
